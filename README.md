# Git Practice Task

This is a simple Hello World Program in C++

## How to Run Code

- First go to green **Clone** option on top right side and copy the URL **Clone with HTTP**
- Here open your Linux `Terminal` and select a `Directory` where you want to work and run following command `git clone < URL >`
- Upon successful clone of repo set working directory to **git-practice-task** using `cd`

### Compile main.cpp file

- Compile main.cpp using the **GCC** compiler using command `g++ -o out main.cpp`
- To view output run command `./out`

### Compile get-string.cpp file

- Compile get-string.cpp using the **GCC** compiler using command `g++ -o output get-string.cpp`
- To view output run command `./output`
